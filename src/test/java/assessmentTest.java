import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import utilities.ExcelUtils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class assessmentTest {
    WebDriver driver;
    private String[][] data = null;
    private String fileName;
    InputStream input = null;
    String appURL;
    String baseUrl;
    Properties EnvironmentProperties;
    ExcelUtils excelUtils;

    @Before
    public void setup() throws Exception {
        EnvironmentProperties = new Properties();
        input = new FileInputStream("src/test/resources/environment.properties");
        EnvironmentProperties.load(input);
        baseUrl = EnvironmentProperties.getProperty("application.url");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        capabilities.setCapability("browserName", EnvironmentProperties.getProperty("browser.name"));


        if( EnvironmentProperties.getProperty("browser.name").equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
            driver = new ChromeDriver();
        }else if( EnvironmentProperties.getProperty("browser.name").equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        fileName = "src/test/resources/testdata.xlsx";
    }

@Test
    public void main() throws Exception {
        EnvironmentProperties.load(input);
        //comment the above 2 lines and uncomment below 2 lines to use Chrome
    //    System.setProperty("webdriver.chrome.driver","c:\\chromedriver.exe");
    //   WebDriver driver = new ChromeDriver();

        String name = "Automation";
        String email = EnvironmentProperties.getProperty("email");
        // launch FF and direct it to the Base URL

        applyOnline(name,email);
     //   driver.close();

    }


    public void applyOnline(String name,String email) throws Exception {
        excelUtils = new ExcelUtils();

        data = excelUtils.readExcelDataFileToArray(fileName, "Sheet1");
        String[] headers = ExcelUtils.getColumHeaders(data);
        for (int i = 1; i < data.length; i++) {
            String jobType = ExcelUtils.getCellValue(data[i], headers, "Job Type");
            String applicantName = ExcelUtils.getCellValue(data[i], headers, "Applicant Name");
            String applicantEmail = ExcelUtils.getCellValue(data[i], headers, "Email");


            driver.findElement(By.xpath("//a[text()='CAREERS']")).click();
            //Thread.sleep(3000);
            driver.findElement(By.xpath("//a[text()='South Africa']")).click();
            Thread.sleep(3000);
            //    driver.findElement(By.xpath("//a[text()='Senior Test Automation Specialist – Johannesburg']")).click();
            driver.findElement(By.xpath("//a[text()='" + jobType +"']")).click();
            //Thread.sleep(5000);
            driver.findElement(By.xpath("//a[contains(@class,'wpjb-form-job-apply')]")).click();

          //  Thread.sleep(5000);
            driver.findElement(By.id("applicant_name")).sendKeys(applicantName);
            driver.findElement(By.id("email")).sendKeys(applicantEmail);

            Random rndNum = new Random();
            String rndNum1 = "";
            Random rndNumlast = new Random();
            String rndNum2 = "";
            for (int nbr = 0; nbr <= 2; nbr++) {
                rndNum1 = String.valueOf(rndNum.nextInt(1000));
            }
            for (int nbr = 0; nbr <= 2; nbr++) {
                rndNum2 = String.valueOf(rndNumlast.nextInt(10000));
            }

            driver.findElement(By.id("phone")).sendKeys("083 " + rndNum1 + " " + rndNum2);
            driver.findElement(By.id("wpjb_submit")).click();
            Thread.sleep(5000);

            if (driver.findElement(By.className("wpjb-errors")).isDisplayed()) {
                System.out.println("Test Passed!!!");
            } else {
                System.out.println("Test Failed");
            }
        }
    }
}
